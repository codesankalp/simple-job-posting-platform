import React from "react";
import backBtn from "../../assets/back.svg";
import {skillOptions, internshipModes} from "../../constants";
import InputRange from "react-input-range";
import "react-input-range/src/scss/index.scss";

class SecondForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      skill: "",
      partTime: false,
      semiFullTime: false,
      fullTime: false,
      stipendRange: {min: 20000, max: 40000},
      date: "",
      duration: "",
      jobDesc: "",
      jobDescPlaceHolder:
        "Enter the job description\nTry to be as precise as possible(250- 300 words)\n1.\n2.\n3.\n",
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleCheckBoxChange = this.handleCheckBoxChange.bind(this);
  }

  componentDidMount() {
    const props = this.props;
    this.setState(props);
  }

  handleSubmit(event) {
    event.preventDefault();
    const {setSecondFormData} = this.props;
    setSecondFormData(this.state, true);
  }

  handleChange(event) {
    this.setState({[event.target.name]: event.target.value});
  }

  handleCheckBoxChange = (event) => {
    this.setState({
      [event.target.name]: event.target.checked,
    });
  };

  render() {
    const {skill, date, duration, jobDesc, jobDescPlaceHolder} = this.state;
    const {toggleModal, setSecondFormData} = this.props;
    return (
      <section className="second-form-container">
        <div className="second-form-header">
          <div
            className="back-btn-container"
            onClick={() => {
              setSecondFormData(this.state);
              toggleModal({firstModal: true, secondModal: false});
            }}
          >
            <img src={backBtn} alt="back btn" />
          </div>
          <h4># Intern Details</h4>
        </div>
        <form className="second-form" onSubmit={this.handleSubmit}>
          <div className="skill-container pb-s">
            <label>Skill</label>
            <input
              type="text"
              name="skill"
              list="skills"
              value={skill}
              onChange={this.handleChange}
              placeholder="Start typing and select the tab"
              required
            />
            <datalist id="skills">
              {skillOptions.map((skill) => {
                return (
                  <option
                    key={skill}
                    value={skill}
                    onClick={() => {
                      this.setState({skill});
                    }}
                  >
                    {skill}
                  </option>
                );
              })}
            </datalist>
          </div>
          <div className="mode-container pb-s">
            <label>Mode</label>
            <div className="internship-modes">
              {internshipModes.map((mode) => {
                return (
                  <span className="part-time" key={mode.id}>
                    <input
                      type="checkbox"
                      name={mode.id}
                      checked={this.state[mode.id]}
                      onChange={this.handleCheckBoxChange}
                    />
                    <label>
                      <b>{mode.name}</b> {mode.time} hrs/week
                    </label>
                  </span>
                );
              })}
            </div>
          </div>
          <div className="slider-container pb-s">
            <label>Stipend Range</label>
            <div className="slider">
              <InputRange
                maxValue={100000}
                minValue={5000}
                formatLabel={(value) => `INR ${value}`}
                value={this.state.stipendRange}
                onChange={(value) => this.setState({stipendRange: value})}
              />
            </div>
          </div>
          <div className="date-duration-container pb-s">
            <span className="date">
              <label>Start Date</label>
              <input
                type="date"
                name="date"
                onChange={this.handleChange}
                value={date}
                required
              />
            </span>
            <span className="duration pb-s">
              <label>Duration</label>
              <div className="inp-dur">
                <input
                  type="number"
                  name="duration"
                  onChange={this.handleChange}
                  value={duration}
                  required
                />
                <span>Months</span>
              </div>
            </span>
          </div>
          <div className="job-description pb-s">
            <label>Job Description</label>
            <textarea
              className="text-box"
              name="jobDesc"
              onClick={(event) => {
                event.target.value =
                  event.target.value === jobDescPlaceHolder ? "" : jobDesc;
              }}
              onChange={this.handleChange}
              value={jobDesc === "" ? jobDescPlaceHolder : jobDesc}
              required
            />
          </div>
          <div className="post-btn-container">
            <button>Post</button>
          </div>
        </form>
      </section>
    );
  }
}

export default SecondForm;

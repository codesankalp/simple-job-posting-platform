import React from "react";
import logo from "../../assets/cuvette_tech_logo.svg";

class Header extends React.Component {
  render() {
    return (
      <div className="header">
        <div className="logo">
          <img src={logo} alt="Cuvette" />
        </div>
        <nav>
          <ul>
            <li>My Listings</li>
          </ul>
        </nav>
      </div>
    );
  }
}

export default Header;

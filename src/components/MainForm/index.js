import React from "react";

class MainForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      location: "",
      remote: false,
      flexible: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleCheckBoxChange = this.handleCheckBoxChange.bind(this);
  }

  componentDidMount() {
    const props = this.props;
    this.setState(props);
  }

  handleSubmit(event) {
    const {title, remote, flexible} = this.state;
    const {setMainFormData, toggleModal} = this.props;
    let {location} = this.state;
    if (event) event.preventDefault();
    if (this.state.remote) {
      location = "";
    }
    if (setMainFormData && toggleModal) {
      setMainFormData({title, location, remote, flexible});
      toggleModal({firstModal: false, secondModal: true});
    }
  }

  handleChange(event) {
    this.setState({[event.target.name]: event.target.value});
  }

  handleCheckBoxChange = (event) => {
    this.setState({
      [event.target.name]: event.target.checked,
    });
  };

  render() {
    const {title, location, remote, flexible} = this.state;
    return (
      <section className="main-form-container">
        <form className="main-form" onSubmit={this.handleSubmit}>
          <div className="job-title-container">
            <label>Job Title</label>
            <input
              name="title"
              value={title}
              onChange={this.handleChange}
              required
            />
          </div>
          <div className="job-location-container">
            <label>Job Location</label>
            <input
              name="location"
              placeholder="Enter the location"
              value={location}
              onChange={this.handleChange}
              disabled={remote}
              required={!remote}
            />
          </div>
          <div className="checkbox-wrapper">
            <div className="space-filler"></div>
            <div className="checkbox-btn-container">
              <div className="checkbox-btn">
                <input
                  type="checkbox"
                  name="remote"
                  checked={remote}
                  onChange={this.handleCheckBoxChange}
                />
                <label>This job is remote</label>
              </div>
              <div className="checkbox-btn">
                <input
                  type="checkbox"
                  name="flexible"
                  checked={flexible}
                  onChange={this.handleCheckBoxChange}
                />
                <label>This job is flexible</label>
              </div>
            </div>
          </div>
          <div className="submit-btn-container">
            <button type="submit">Next</button>
          </div>
        </form>
      </section>
    );
  }
}

export default MainForm;

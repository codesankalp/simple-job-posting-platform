import React from "react";

class Detail extends React.Component {
  render() {
    const {toggleModal, job} = this.props;
    return (
      <div className="detail-container">
        <div
          className="close-modal-btn"
          onClick={() => {
            toggleModal({detail: null, openModal: false});
          }}
        >
          x
        </div>
        <div className="job-title">
          <div>{job.title}</div>
        </div>
        <div className="cen-align">
          <div className="">Job Location</div>
          <div className="bold">{job.remote ? "Remote" : job.location}</div>
        </div>
        <div className="cen-align">
          <div>Skill</div>
          <div className="bold">{job.skill}</div>
        </div>
        <div className="cen-align">
          <div>Stipend Range</div>
          <div className="bold">
            INR {job.stipendRange.min} - INR {job.stipendRange.max}
          </div>
        </div>
        <div className="cen-align">
          <div>Modes</div>
          <div className="fs-sm bold">
            {job.partTime ? "Part Time," : null}{" "}
            {job.fullTime ? "Full Time," : null}{" "}
            {job.semiFullTime ? "Semi Full Time" : null}
          </div>
        </div>
        <div className="cen-align fs-sm bold">
          <div>{job.flexible ? "This vacancy is flexible" : null}</div>
        </div>
        <div className="cen-align fs-sm">
          <pre>{job.jobDesc}</pre>
        </div>
      </div>
    );
  }
}

export default Detail;

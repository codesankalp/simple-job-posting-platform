import React from "react";
import Header from "../Header";
import JobsList from "../JobsList";
import MainForm from "../MainForm";
import SecondForm from "../SecondForm";
import initialJobs from "../../initialJobs.json";

class Wrapper extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openModal: false,
      allJobs: [],
      firstModal: false,
      secondModal: false,
      mainFormData: {},
      secondFormData: {},
    };
    this.setMainFormData = this.setMainFormData.bind(this);
    this.setSecondFormData = this.setSecondFormData.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.addJobBtn = this.addJobBtn.bind(this);
    this.createJob = this.createJob.bind(this);
    this.updateJobs = this.updateJobs.bind(this);
  }

  componentDidMount() {
    const allJobs = JSON.parse(localStorage.getItem("jobs"));
    if (allJobs === null || allJobs.length === 0) {
      this.updateJobs(initialJobs);
      return;
    }
    this.updateJobs(allJobs);
  }

  updateJobs(data) {
    this.setState({
      allJobs: data,
    });
    localStorage.setItem("jobs", JSON.stringify(data));
  }

  addJobBtn() {
    this.setState({openModal: true, firstModal: true});
  }

  toggleModal(state) {
    this.setState(state);
  }

  setMainFormData(data) {
    this.setState({mainFormData: data});
  }

  setSecondFormData(data, create = false) {
    this.setState({secondFormData: data});
    if (create) this.createJob(data);
  }

  createJob(data) {
    const {mainFormData} = this.state;
    const allJobs = JSON.parse(localStorage.getItem("jobs"));
    allJobs.push({id: `${Math.random()}`, ...mainFormData, ...data});
    this.updateJobs(allJobs);
    this.setState({
      openModal: false,
      firstModal: false,
      secondModal: false,
      mainFormData: {},
      secondFormData: {},
      detail: null,
    });
  }

  render() {
    const {
      openModal,
      allJobs,
      firstModal,
      secondModal,
      mainFormData,
      secondFormData,
      detail,
    } = this.state;
    return (
      <>
        <div className={openModal ? "modal-wrapper active" : "modal-wrapper"}>
          <div className="modal">
            {firstModal ? (
              <MainForm
                {...mainFormData}
                setMainFormData={this.setMainFormData}
                toggleModal={this.toggleModal}
              />
            ) : null}
            {secondModal ? (
              <SecondForm
                {...secondFormData}
                setSecondFormData={this.setSecondFormData}
                toggleModal={this.toggleModal}
              />
            ) : null}
            {detail === null ? null : detail}
          </div>
        </div>
        <div className="container">
          <Header />
          <hr className="hr-1" />
          <JobsList
            jobs={allJobs}
            addJob={this.addJobBtn}
            toggleModal={this.toggleModal}
          />
        </div>
      </>
    );
  }
}

export default Wrapper;

import React from "react";
import Detail from "../Detail";

class JobsList extends React.Component {
  render() {
    const {jobs, addJob, toggleModal} = this.props;
    return (
      <div className="list-container">
        <div className="center-div">
          <div className="add-job">
            <div className="ml-auto" onClick={addJob}>
              + Add another job
            </div>
          </div>
          <div className="scroll">
            <table>
              <tbody>
                {jobs.map((job, index) => (
                  <tr key={job.id}>
                    <td colSpan={8} className="wrap">
                      {index + 1}. {job.title}
                    </td>
                    <td colSpan={4}>{job.date}</td>
                    <td
                      colSpan={4}
                      className="fs-20"
                      onClick={() => {
                        toggleModal({
                          openModal: true,
                          detail: (
                            <Detail toggleModal={toggleModal} job={job} />
                          ),
                        });
                      }}
                    >
                      Details
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default JobsList;

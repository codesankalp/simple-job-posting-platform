export const skillOptions = [
  "NodeJS",
  "ReactJS",
  "Angular",
  "Python",
  "Java",
  "Flutter",
  "HTML",
  "CSS",
  "Android",
];

export const internshipModes = [
  {id: "partTime", name: "Part-time", time: "20"},
  {id: "semiFullTime", name: "Semi Full-time", time: "30"},
  {id: "fullTime", name: "Full-time", time: "40"},
];
